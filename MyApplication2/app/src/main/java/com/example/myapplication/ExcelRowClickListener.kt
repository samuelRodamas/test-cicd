package com.example.myapplication

interface ExcelRowClickListener {
    fun onRowClick(position: Int)
}