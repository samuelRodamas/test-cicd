package com.example.myapplication

import android.content.Context
import android.os.Environment
import android.widget.Toast
import com.example.myapplication.model.CustomerModel
import org.apache.poi.ss.usermodel.BorderStyle
import org.apache.poi.ss.usermodel.FillPatternType
import org.apache.poi.ss.usermodel.HorizontalAlignment
import org.apache.poi.ss.usermodel.IndexedColors
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale


/**
 * Created by samuel.septiano on 03/05/2023.
 */
class WriteFileToExcel {
     fun createXlsx(context: Context, modelHistoryAbsen: List<CustomerModel>) {
        try {
            val strDate: String =
                SimpleDateFormat("dd-MM-yyyy HH-mm-ss", Locale.getDefault()).format(Date())
            val root = File(
                Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "FileExcel"
            )
            if (!root.exists()) root.mkdirs()
            val path = File(root, "/$strDate.xlsx")
            val workbook = XSSFWorkbook()
            val outputStream = FileOutputStream(path)
            val headerStyle = workbook.createCellStyle()
            headerStyle.setAlignment(HorizontalAlignment.CENTER)
            headerStyle.fillForegroundColor = IndexedColors.BLUE_GREY.getIndex()
            headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND)
            headerStyle.setBorderTop(BorderStyle.MEDIUM)
            headerStyle.setBorderBottom(BorderStyle.MEDIUM)
            headerStyle.setBorderRight(BorderStyle.MEDIUM)
            headerStyle.setBorderLeft(BorderStyle.MEDIUM)
            val font = workbook.createFont()
            font.fontHeightInPoints = 12.toShort()
            font.color = IndexedColors.WHITE.getIndex()
            font.bold = true
            headerStyle.setFont(font)
            val sheet = workbook.createSheet("Data Movie")
            var row = sheet.createRow(0)
            var cell = row.createCell(0)
            cell.setCellValue("Judul Film")
            cell.cellStyle = headerStyle
            cell = row.createCell(1)
            cell.setCellValue("Tanggal Rilis")
            cell.cellStyle = headerStyle
            cell = row.createCell(2)
            cell.setCellValue("Deskripsi")
            cell.cellStyle = headerStyle
            for (i in modelHistoryAbsen.indices) {
                row = sheet.createRow(i + 1)
                cell = row.createCell(0)
                cell.setCellValue(modelHistoryAbsen[i].name)
                sheet.setColumnWidth(0,
                    (modelHistoryAbsen[i].name?.length?.plus(30))?.times(256) ?: 0
                )
                cell = row.createCell(1)
                cell.setCellValue(modelHistoryAbsen[i].address)
                sheet.setColumnWidth(1, modelHistoryAbsen[i].address?.length?.times(256) ?: 0)
                cell = row.createCell(2)
                cell.setCellValue(modelHistoryAbsen[i].age.toString())
            }
            workbook.write(outputStream)
            outputStream.close()
            Toast.makeText(context, "Data berhasil di ekspor!", Toast.LENGTH_SHORT).show()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

}